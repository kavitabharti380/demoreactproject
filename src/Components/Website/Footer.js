
import React, { useState } from 'react';
import { FormControl, InputGroup } from 'react-bootstrap';
import '../WebsiteBuilder/sidebar.css';
import './website.css';
import imgbnr from '../Images/banner.jpg'


const Footer = (props) => {
    
    /*  THIS FUNCTION FOR INPUT TEXT          */
    const [user_name, setUsername] = useState("Abhay Braja's School 2021")
    const [msg, setMsg] = useState("Abhay Braja's School 2021")

    const fillValue = (event) => {

        if(event.target.name === 'user_name'){
            setUsername(event.target.value)
        }
        else{
            setMsg(event.target.value)
        }
        
        event.preventDefault();
    }
    
    return (
        <>
            <div className="row">
                <div className="col-md-3" style={{ backgroundColor: '#f1f1f1', }}>
                    <div className="" style={{ color: '#111', }}>
                        <div className="">
                            <InputGroup className="mb-2">
                                <FormControl name="user_name" className={'input-flat'} size='sm' 
                                defaultValue={"Abhay Braja's School 2021"}
                                    onChange={fillValue}
                                />
                            </InputGroup>
                            <InputGroup className="mb-2">
                                <FormControl name="msg" className={'input-flat'} size='sm'
                                defaultValue={"msg"}
                                    onChange={fillValue}
                                />
                            </InputGroup>

                        </div>
                    </div>
                </div>
                <div className="col-md-9 p-0">
                    <div className="row bg-dark pt-5">
                        <div className="col-md-5">
                            <p style={{ color: "white", marginTop: "18px", fontSize: "15px", fontWeight: "normal" }}>
                                {user_name}  </p>
                        </div>
                        <div className="col-md-5">

                        </div>
                        <div className="col-md-2">
                            <p style={{ color: "white", marginTop: "18px", fontSize: "12px" }} >Terms of Use <br />Privacy Policy</p>
                            <p style={{ color: "white", fontSize: "12px", fontWeight: "normal", fontFamily: "sans-serif" }}>Teach Online with <span style={{ fontWeight: "bold", fontSize: "17px" }}>{msg}</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </>

    );
}

export default Footer;
