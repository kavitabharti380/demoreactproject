
import React, { useState } from 'react';
import { FormControl, InputGroup } from 'react-bootstrap';
import '../WebsiteBuilder/sidebar.css';
import './website.css';
import imgbnr from '../Images/banner.jpg'
import newimg from '../Images/text-img1.jpg'
import newimg2 from '../Images/text-img2.jpg'
import newimg3 from '../Images/text-img3.jpg'

const Section2 = (props) => {
   
    /*  THIS FUNCTION FOR INPUT TEXT          */
    const [banner, setBanner] = useState(imgbnr)
    const [exmtitle1, setExmtitle1] = useState('Example1')
    const [exmtitle2, setExmtitle2] = useState('Example2')
    const [exmtitle3, setExmtitle3] = useState("Example3")
    const [desc1, setDesc1] = useState("Use this block to showcase testimonials, features, categories, or more. Each column has its own individual text field. You can also leave the text blank to have it display nothing and just showcase an image.")
    const [desc2, setDesc2] = useState("Use this block to showcase testimonials, features, categories, or more. Each column has its own individual text field. You can also leave the text blank to have it display nothing and just showcase an image.")
    const [desc3, setDesc3] = useState("Use this block to showcase testimonials, features, categories, or more. Each column has its own individual text field. You can also leave the text blank to have it display nothing and just showcase an image.")


    function setImageInView(val) {
        var img_url = URL.createObjectURL(val.target.files[0])
        setBanner(img_url);
        props.handleCallback(img_url, 'backimg')
    }

    const fillValue = (event) => {
        if(event.target.name === 'title1'){
            setExmtitle1(event.target.value);
        }
        else if(event.target.name === 'title2'){
            setExmtitle2(event.target.value);
        }
        else if(event.target.name === 'title3'){
            setExmtitle3(event.target.value);
        }
        event.preventDefault();
       if(event.target.name === 'desc1'){
            setDesc1(event.target.value);
        }

        else if(event.target.name === 'desc2'){
            setDesc2(event.target.value);
        }
        else if(event.target.name === 'desc3'){
            setDesc3(event.target.value);
        }
        
        event.preventDefault();
    }



    return (
        <>
            <div className="row">
                <div className="col-md-3" style={{ backgroundColor: '#f1f1f1', }}>
                    <div className="" style={{ color: '#111', }}>
                        <div className="">
                            <InputGroup className="mb-2">
                                <FormControl name="title1" className={'input-fat'} size="sm"
                                defaultValue={"Example Title"}
                                    onChange={fillValue}
                                />
                            </InputGroup>

                            <InputGroup className="mb-2">
                                <FormControl name="desc1" as="textarea" rows={1} className={'input-fat'} size="sm" 
                                defaultValue={"Use this block to showcase testim"}
                                    onChange={fillValue} />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <FormControl name="desc" type="file" className={'input-fat'} size="sm" ng-model="image" accept="image/*"
                                    onChange={setImageInView} />
                            </InputGroup>
                            <InputGroup className="mb-2">
                                <FormControl name="title2" className={'input-fat'} size="sm" defaultValue={"Example Title"}
                                    onChange={fillValue}
                                />
                            </InputGroup>

                            <InputGroup className="mb-2">
                                <FormControl name="desc2" as="textarea" rows={1} className={'input-fat'} size="sm" defaultValue={"A short, descriptive sentence about your school."}
                                    onChange={fillValue} />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <FormControl name="desc" type="file" className={'input-fat'} size="sm" ng-model="image" accept="image/*"
                                    onChange={setImageInView} />
                            </InputGroup>
                            <InputGroup className="mb-2">
                                <FormControl name="title3" className={'input-fat'} size="sm" defaultValue={"Example Title"}
                                    onChange={fillValue}
                                />
                            </InputGroup>

                            <InputGroup className="mb-2">
                                <FormControl name="desc3" as="textarea" rows={1} className={'input-fat'} size="sm" defaultValue={"A short, descriptive sentence about your school."}
                                    onChange={fillValue} />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <FormControl name="desc" type="file" className={'input-fat'} size="sm" ng-model="image" accept="image/*"
                                    onChange={setImageInView} />
                            </InputGroup>
                        </div>
                    </div>
                </div>
                <div className="col-md-9 p-0">
                    <div className="row text-center">
                        <div className="col-md-4">
                            <img src={newimg} style={{ width: '100%' }}></img>

                            <h6 className="mt-5">{exmtitle1}</h6>

                            <p style={{ fontSize: "12px", color: "#677079", padding: "8px", fontFamily: "Lato,sans-serif" }}>
                                {desc1}
                            </p>

                        </div>
                        <div className="col-md-4">
                            <img src={newimg2} style={{ width: '100%' }}></img>
                            <h6 className="mt-5"> {exmtitle2}</h6>

                            <p style={{ fontSize: "12px", color: "#677079", padding: "8px", fontFamily: "Lato,sans-serif" }}>
                                {desc2}
                            </p>

                        </div>
                        <div className="col-md-4">
                            <img src={newimg3} style={{ width: '100%' }}></img>
                            <h6 className="mt-5"> {exmtitle3}</h6>

                            <p style={{ fontSize: "12px", color: "#677079", padding: "8px", fontFamily: "Lato,sans-serif" }}>
                               {desc3}
                            </p>

                        </div>

                    </div>
                </div>
            </div>
        </>

    );
}

export default Section2;
