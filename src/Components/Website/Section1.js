
import React, { useState } from 'react';
import { FormControl, InputGroup } from 'react-bootstrap';
import '../WebsiteBuilder/sidebar.css';
import './website.css';
import imgbnr from '../Images/gril.jpg'

const Section1 = () => {
   
    /*  THIS FUNCTION FOR INPUT TEXT          */
    const [bannerimg, setBanner] = useState(imgbnr)
    const [quote, setQuote] = useState('"Include a testimonial sharing why your students love your content."')
    const [writer, setWriter] = useState("-Jane Doe")

    function setImageInView(val) {
        var img_url = URL.createObjectURL(val.target.files[0])
        setBanner(img_url);
    }

    const fillValue = (event) => {

        if(event.target.name === 'quote'){
            setQuote(event.target.value)
        }
        else{
            var w = event.target.value
            setWriter('-'+w)
        }
        
        event.preventDefault();
    }

    return (
        <>
            <div className="row">
                <div className="col-md-3" style={{ backgroundColor: '#f1f1f1', }}>
                    <div style={{ color: '#111', marginTop:'200px'}}>
                        <div className="">
                            <InputGroup className="mb-3">
                                <FormControl name="quote" as="textarea" rows={3} className={'input-fat'} size='sm' 
                                defaultValue={'"Include a testimonial sharing why your students love your content."'}
                                    onChange={fillValue} />
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <FormControl name="schoolName" className={'input-fat'} size='sm' defaultValue={"Jane Doe"}
                                    onChange={fillValue}
                                />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <FormControl name="desc" type="file" rows={3} className={'input-fat'} size='sm' ng-model="image" accept="image/*"
                                    onChange={setImageInView} />
                            </InputGroup>
                        </div>
                    </div>
                </div>
                <div className="col-md-9 p-0">
                    <div className="row" id='section2'>
                        <div className="col-md-6" style={{ height: '600px' }}>
                            <div className="text-center " style={{ marginTop: '70%' }}>
                                <p style={{ fontSize: "18px", color: "#57616b" }} >
                                {quote}
                                </p>
                                <p style={{ fontSize: "13px", color: "#57616b" }}>
                                {writer}
                                </p>
                            </div>

                        </div>
                        <div className="col-md-6 py-4" >
                            <div className="home-img1" style={{ height: '95%', backgroundImage: `url(${bannerimg})` }}>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
        </>

    );
}

export default Section1;
