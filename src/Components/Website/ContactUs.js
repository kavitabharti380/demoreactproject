
import React, { useState } from 'react';
import { Form, FormControl, InputGroup } from 'react-bootstrap';
import '../WebsiteBuilder/sidebar.css';
import './website.css';


const ContactUs = (props) => {
   
    /*  THIS FUNCTION FOR INPUT TEXT */
    var [msg, setMsg] = useState('We respect your privacy.')

    const fillValue = (event) => {
        setMsg(event.target.value)
    }

    return (
        <>
            <div className="row">
                <div className="col-md-3" style={{ backgroundColor: '#f1f1f1', }}>
                    <div className="" style={{ color: '#111', }}>
                        <div className="" style={{marginTop:'340px'}}>
                            <InputGroup className="mb-3">
                                <FormControl name="message" className={'input-flat'} size='sm'
                                 defaultValue={"We respect your privacy."}
                                    onChange={fillValue}
                                />
                            </InputGroup>

                        </div>
                    </div>
                </div>
                <div className="col-md-9 p-0">
                    <div className="row bg-white ">
                        <div className="col-md-8 offset-md-2 mt-5">
                            <h5 className="text-center">
                                Let's keep in touch
                            </h5>
                            <p style={{
                                        color: "#5e6770", fontFamily: " sans-serif", fontSize: "16px",
                                        margin: "12px", textAlign: "center", fontWeight: "normal"
                                 }}>
                                Subscribe to the mailing list and receive the latest updates
                            </p>
                            <div >
                                <Form className=" mt-4 ">
                                    <label for="exampleFormControlInput1" class="form-label ">Email address</label>
                                    <input type="email" class="form-control" id="exampleFormControlInput1" />

                                </Form>
                            </div>
                            <div class="form-check mt-4">
                                <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                <label class="form-check-label" for="flexCheckDefault">
                                    <p style={{ fontSize: "15px", color: "#5e6770", fontFamily: "sans-serif" }}>
                                        By clicking this checkbox, you consent to receiving emails from this <br />school or course

                             </p>
                                </label>
                            </div>
                            <div className="text-center">
                                <button className="keep-sub-btn ">Subscribe</button>
                                <p style={{ fontSize: "13px", fontWeight: "normal", marginTop: "25px" }}>{msg}</p>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>

    );
}

export default ContactUs;
