
import React, { useState } from 'react';
import {FormControl, InputGroup } from 'react-bootstrap';
import '../WebsiteBuilder/sidebar.css';
import './website.css';
import imgbnr from '../Images/gril.jpg'



const Section3 = (props) => {
    
    /*  THIS FUNCTION FOR INPUT TEXT          */
    var [banner, setBanner] = useState(imgbnr)
    var [user_desc, setUserDesc] = useState('"Use this block for your bio. Explain to your audience who you are and why you’re teaching this course."')
    var [user_name, setUsername] = useState("Hi, I’m [Your Name Here]")

    function setImageInView(val) {
        var img_url = URL.createObjectURL(val.target.files[0])
        setBanner(img_url);
    }

    const fillValue = (event) => {

        if(event.target.name === 'user_desc'){
            setUserDesc(event.target.value)
        }
        else{
            setUsername(event.target.value)
        }
        
        event.preventDefault();
    }

    return (
        <>
            <div className="row">
                <div className="col-md-3" style={{ backgroundColor: '#f1f1f1', }}>
                    <div className="mt-5" style={{ color: '#111', }}>
                        <div className="">

                            <InputGroup className="mb-3">
                                <FormControl name="img3" type="file" rows={3} className={'input-fat'} size="sm" ng-model="image" accept="image/*"
                                    onChange={setImageInView} />
                            </InputGroup>
                            <InputGroup className="mb-3">
                                <FormControl name="user_name" className={'input-fat'} size="sm" defaultValue={"Your Name Here"}
                                    onChange={fillValue}
                                />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <FormControl name="user_desc" as="textarea" rows={3} className={'input-fat'} size="sm"
                                defaultValue={"Use this block for your bio. Explain to your audience who you are, and why you’re teaching thi"}
                                    onChange={fillValue} />
                            </InputGroup>

                           
                        </div>
                    </div>
                </div>
                <div className="col-md-9 p-0">
                    <div className="row">

                        <div className="col-md-6 py-4 " >
                            <div className="home-img1" style={{ height: '95%', backgroundImage: `url(${banner})` }}>

                            </div>

                        </div>
                        <div className="col-md-6" style={{ height: '600px' }}>
                            <div className="text-center " style={{ marginTop: '50%' }}>
                                <h5 className="fw-normal">
                                    {user_name}
                                </h5>
                                <p style={{ fontSize: "12px", color: "#57616b" }} >
                                    {user_desc}
                                </p>

                            </div>

                        </div>



                    </div>
                </div>
            </div>
        </>

    );
}

export default Section3;
