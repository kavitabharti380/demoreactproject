
import React, { useState } from 'react';
import { Form, FormControl, InputGroup } from 'react-bootstrap';
import '../WebsiteBuilder/sidebar.css';
import './website.css';
import { Row, Col } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserCircle } from '@fortawesome/free-solid-svg-icons'
import imgbnr from '../Images/banner.jpg'

const Crousal = (props) => {
    
    /*  THIS FUNCTION FOR INPUT TEXT          */
    var [banner, setBanner] = useState(imgbnr)
    var [name, setName] = useState('School Name')
    var [desc, setDesc] = useState('Description')

    function setImageInView(val) {
        var img_url = URL.createObjectURL(val.target.files[0])
        setBanner(img_url);
    }

    const fillValue = (event) => {

        if(event.target.name === 'schoolName'){
            setName(event.target.value)
        }
        else if(event.target.name === 'desc'){
            setDesc(event.target.value)
        }
        
        event.preventDefault();
    }

    return (
        <div className="row">
            <div className="col-md-3" style={{ backgroundColor: '#f1f1f1', }}>
                <div className="pt-5" style={{ color: '#111', }}>
                    <div className="">
                        <Form.Label htmlFor="basic-url">Header</Form.Label>
                        <InputGroup className="mb-3">
                            <FormControl name="schoolName" size="sm" aria-describedby="basic-addon3" 
                            defaultValue={name}
                                onChange={fillValue} className={'input-fat'}
                            />
                        </InputGroup>

                        <InputGroup className="mb-3">
                            <FormControl className={'input-fat'} size="sm" name="desc" as="textarea" rows={2}  
                            defaultValue={desc}
                                onChange={fillValue} />
                        </InputGroup>

                        <InputGroup className="mb-3">
                            <FormControl className={'input-fat'} size="sm" name="desc" type="file" rows={3} id="basic-url" ng-model="image" accept="image/*"
                                onChange={setImageInView} />
                        </InputGroup>
                    </div>
                </div>
            </div>
            <div className="col-md-9 p-0">
                <div className="">
                    <Row>
                        <Col className="h60 head-color">
                            <div className="v-center">
                                <Row >
                                    <Col>Abhay Braja's School</Col>
                                    <Col md="auto">
                                        <p className="fs12 verticle-middle">My Products</p>
                                    </Col>
                                    <Col md="auto">
                                        <p className="fs12 verticle-middle">All Products</p>
                                    </Col>
                                    <Col md="auto">
                                        <div >
                                            <FontAwesomeIcon icon={faUserCircle} size="2x" />

                                        </div>
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col className="h350 home-img" style={{ backgroundImage: `url(${banner})` }}>

                            <div className="text-center" style={{ color: 'white', marginTop: "80px" }}>
    <h1 >{name}</h1>
                                <p className="fs-5 mt-3">{desc}</p>
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </div>

    );
}

export default Crousal;
