
import React, { useState } from 'react';
import '../WebsiteBuilder/sidebar.css';
import './website.css';
import NavSidebar from '../WebsiteBuilder/NavSidebar';
import Crousal from '../Website/Crousal';
import Description from '../Website/Description';
import Section1 from '../Website/Section1';
import Section2 from '../Website/Section2';
import Section3 from '../Website/Section3';
import ContactUs from '../Website/ContactUs';
import Footer from '../Website/Footer';

const Parent = (props) => {


    return (

        <>
            <div className="colmd1 bg-clr p-0">
                <NavSidebar />
            </div>
            <div className="colmd11 offsetmd1">
                <Crousal />
                <Description />
                <Section1 />
                <Section2 />
                <Section3 />
                <ContactUs />
                <Footer />

            </div>

        </>

    );
}

export default Parent;
