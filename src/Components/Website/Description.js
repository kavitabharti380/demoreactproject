
import React, { useState } from 'react';
import { Form, FormControl, InputGroup } from 'react-bootstrap';
import '../WebsiteBuilder/sidebar.css';
import './website.css';
import imgbnr from '../Images/banner.jpg'

const Description = (props) => {
    
    var [desc1, setDesc1] = useState('Describe what your school is about, what your students will learn and why someone should buy your courses.')
    var [desc2, setDesc2] = useState('Note that signing up for your school will add students to your Users list, but they will still need to enroll in specific courses and coaching products of their choice to access your content.')

    const fillValue = (event) => {

        if(event.target.name === 'desc1'){
            setDesc1(event.target.value)
        }
        else{
            setDesc2(event.target.value)
        }
        
        event.preventDefault();
    }


    return (
        <>
            <div className="row">
                <div className="col-md-3" style={{ backgroundColor: '#f1f1f1', }}>
                    <div className="" style={{ color: '#111', }}>
                        <div className="mt-3">
                            <InputGroup className="mb-3">
                                <FormControl name="desc1" as="textarea" rows={4} className={'input-fat'} size='sm' 
                                defaultValue={desc1}
                                    onChange={fillValue} />
                            </InputGroup>

                            <InputGroup className="mb-3">
                                <FormControl name="desc2" as="textarea" rows={4} className={'input-fat'} size='sm' 
                                defaultValue={desc2}
                                    onChange={fillValue} />
                            </InputGroup>

                        </div>
                    </div>
                </div>
                <div className="col-md-9 p-0">
                    <div className="row" style={{ backgroundColor: "#f5f5f8" }}>
                        <div className="col-md-8 offset-md-2 mb-5 pt-5">
                            <p style={{ fontSize: "14px", }}>
                                {desc1}
                            </p>
                            <p style={{ fontSize: "14px", fontWeight: "bold" }}>
                            {desc2}
                            </p>


                        </div>

                    </div>
                </div>
            </div>
        </>

    );
}

export default Description;
