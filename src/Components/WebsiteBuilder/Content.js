import React from 'react'
import Uploader from './Uploader'
import Heading from './Heading'
import Subheading from './Sub-heading'
import ColorPalette from './ColorPalette'
import Footer from './Footer'


const Content=()=> {
    return (
        <div className="col-md-12 p-4 pb-0 mt-5">
            <div className="mx-4">
                <Heading text="Logo & Branding"/>
                <Subheading text="Add a custom logo and/or favicon, and adjust your school thumbnail."/>
                <Uploader/>
            </div>
            
            <hr className={"my-5 text-muted"}></hr>
            <div className="mx-4">
                <Heading text="Color Palette"/>
                <Subheading text="This is a set of colors used throughout your site and products. 
                Choose a preset to get started. You can also override and choose your own colors below."/>
                <ColorPalette/>
                
            </div>
            <hr className={"my-3 text-muted"}></hr>
            <Footer/>
        </div>
    );
}

export default Content;
