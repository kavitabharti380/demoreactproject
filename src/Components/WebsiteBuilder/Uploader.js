import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons'
import UploadFile from './UploadFile'


const Uploader = () => {
    return (
        <div className="row mt-3">

            <div className="col-md-4">
                <p className="text-muted " style={{ fontSize: "13px" }}>
                    Logo & Branding
                    <FontAwesomeIcon icon={faQuestionCircle}
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Your logo appears in the
                            top left of all the pages 
                            in your school"
                        style={{ fontSize: "12px", marginLeft: "5", marginBottom: "1" }}
                    />
                </p>
                <UploadFile id="logo" title="Upload your logo"
                    formate="Recommended format: 250 x 60 pixels (PNG or JPG)" height={250} width={60} />

            </div>

            <div className="col-md-4">
                <p className="text-muted " style={{ fontSize: "13px" }}>
                    School Thumbnail
                    <FontAwesomeIcon icon={faQuestionCircle}
                        data-bs-toggle="tooltip" title="Tooltip on bottom"
                        style={{ fontSize: "12px", marginLeft: "5", marginBottom: "1" }}
                    />
                </p>
                <UploadFile id="thumbnail" title="Upload your thumbnail"
                    formate="Recommended format: 960 x 540 pixels  (PNG or JPG)" height={960} width={540}/>


            </div>

            <div className="col-md-4">
                <p className="text-muted " style={{ fontSize: "13px" }}>
                    Favicon
                    <FontAwesomeIcon icon={faQuestionCircle}
                        data-bs-toggle="tooltip" data-bs-placement="bottom" title="Tooltip on bottom"
                        style={{ fontSize: "12px", marginLeft: "5", marginBottom: "1" }}
                    />
                </p>
                <UploadFile id="favicon" title="Upload your favicon"
                    formate="Recommended format: 32 x 32 pixels (PNG or JPG)" height={32} width={32}/>


            </div>

        </div>
    )
}

export default Uploader;
