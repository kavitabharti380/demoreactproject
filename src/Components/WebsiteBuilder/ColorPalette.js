import React from 'react'
import Preset from './Preset'
import PresetColor from './PresetColor'
import { Button } from 'react-bootstrap'

const ColorPalette = () => {
    return (
        <div>

            <div className="row g-3">
                <div className="col-3">
                    <Preset title="Preset 1" c1="#111" c2="#134c61" />
                </div>

                <div className="col-1"></div>

                <div className="col-3">
                    <Preset title="Preset 2" c1="#ff3f20" c2="#09a59a" />
                </div>

                <div className="col-1"></div>

                <div className="col-3">
                    <Preset title="Preset 3" c1="#111" c2="#134c61" />
                </div>
                <div className="col-md-3">
                    <Preset title="Preset 4" c1="#111" c2="#134c61" />

                </div>

            </div>
            <div className="row g-3 mt-3">
                <div className="col-3">
                    <PresetColor title="Nav Bar & Footer Background" c1="#111" text="Fixed, scrolling & email" />
                </div>

                <div className="col-1"></div>

                <div className="col-3">
                    <PresetColor title="Navigation Bar Link" c1="#ff3f20" text="Links when nav bar is fixed" />
                </div>

                <div className="col-1"></div>

                <div className="col-3">
                    <PresetColor title="Navigation Bar link" c1="#111" text="Links when nav bar is scrolling" />
                </div>

            </div>
            <div className="row g-3 mt-3">
                <div className="col-3">
                    <PresetColor title="Homepage Headings & Subtitle" c1="#111" text="When a background is set" />
                </div>

                <div className="col-1"></div>

                <div className="col-3">
                    <PresetColor title="Course Page Heading & Subtitle" c1="#ff3f20" text="When a Background is set" />
                </div>

                <div className="col-1"></div>

                <div className="col-3">
                    <PresetColor title="Headings" c1="#111" text="<h2>, <h3>, <h4>, <h5>" />
                </div>

            </div>
            <div className="row g-3 mt-3">
                <div className="col-3">
                    <PresetColor title="Body text" c1="#111" text="<Body>, <p>" />
                </div>

                <div className="col-1"></div>

                <div className="col-3">
                    <PresetColor title="Buttons & Links" c1="#ff3f20" text="<a>, <button>" />
                </div>


            </div>
            <div className="row mt-3 p-2">
                <div className="col-12 text-end">
                    <Button variant="outline-secondary" style={{width: '12%', color:"black", fontWeight:"bold", fontSize:"13px"}} > Save</Button>
                </div>
            </div>
            

        </div>

    )
}

export default ColorPalette;
