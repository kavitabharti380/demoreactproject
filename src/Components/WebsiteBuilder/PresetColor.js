import React from 'react'
import { FormControl, InputGroup } from 'react-bootstrap'
import './cardsec.css'

function PresetColor(props) {
    return (
        <div className="row" style={{backgroundColor: "#fafafa"}}>
            <div className="col-md-12 p-0">
                <h6 className={"text-sm"}>{props.title}</h6>
                <p className={"text-xsm"}>{props.text}</p>
            </div>

            <div className="col-md-12 p-1 text-center border-xsm h80">
                <div className="row g-0">
                    <div className="col-3" >
                        <div style={{ backgroundColor: props.c1, height: "100%" }}>
                        </div>
                    </div>
                    <div className="col-9 text-center p-2 px-3">
                    <InputGroup className="">
                        <FormControl
                            placeholder="Color code"
                            aria-label="Color code"
                            plaintext="true"
                            aria-describedby="basic-addon1"
                            defaultValue={props.c1}
                        />
                        </InputGroup>
                    </div>
                    
                </div>

            </div>
        </div>
    )
}

export default PresetColor;
