import React from 'react';
import { Button, Image as BImage } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faImage } from '@fortawesome/free-solid-svg-icons';
import './cardsec.css';



class UploadFile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            file: null
        };
        this.handleChange = this.handleChange.bind(this)
    }

    // manages document state
    componentDidMount() {
        console.log("componentDidMount")
        this.fileSelector = document.getElementById(this.props.id);
    }

    handleFileSelect = (e) => {
        // clicked by button
        e.preventDefault();
        this.fileSelector.click();
        // clicking input tag
    }

    

    handleChange(event) {
        var s = this.props
        const updateImage=(file)=>{
            this.setState({
                file: URL.createObjectURL(file)
            })
        }
        // set state : file 
        const files = event.target.files

        var image = new Image();

        //Set the Base64 string return from FileReader as source.
        image.src = URL.createObjectURL(files[0]);

        //Validate the File Height and Width.
        image.onload = function (props) {
            var height = this.height;
            var width = this.width;
            // if (height > s.height || width > s.width) {
            //     alert("Please upload correct image dimentions ");
            //     return false;
            // }
            alert("Uploaded image has valid Height and Width.");
            updateImage(event.target.files[0])
            return true;

        };
    }

    render() {
        return (
            <div className="row p-2">
                <div className="col-md-12 py-4 px-4 text-center border-sm h220">
                    <input id={this.props.id} type="file" onChange={this.handleChange} style={{ display: 'none' }} />
                    {
                        this.state.file != null ?
                            <BImage src={this.state.file} style={{ height: '40%', width: 'auto' }} />
                            : <> <FontAwesomeIcon className="mt-3" icon={faImage} size='2x' color="#d4d6d8" />
                                <h6 className={"text-sm mt-3 "}>{this.props.title}</h6></>
                    }

                    <p className={"spn-txt mt-0 text-muted"}>{this.props.formate}</p>

                    {/* <Button  onClick={this.handleFileSelect} variant="outline-secondary" className="btn-upload"  > Upload </Button> */}
                    <Button onClick={this.handleFileSelect} variant="outline-secondary" className="btn1-upload" size="sm" > Upload </Button>

                </div>

            </div>
        )
    }
}

export default UploadFile;
