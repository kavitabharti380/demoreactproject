
import React from 'react';
import {NavLink } from 'react-router-dom'
import './sidebar.css'

const NavSidebar = (props) => {
    return (

        <>
            <div className="h60 border-bottom" style={{ position: 'relative' }}>
                <p className="logo v-center" style={{fontSize:"13px"}}>Abhay Braja's School</p>
            </div>
            <div>

                <ul className="lst-cntr p-0 mt-3">
                    <ol className="p-1 nav-item mb-1 active">
                        <NavLink className="a-p" to="/" >Home</NavLink>
                    </ol>
                    <ol className="p-1 nav-item">
                        <NavLink className="a-p" to="/edit" >Edit</NavLink>
                    </ol>
                   
                </ul>

            </div>


        </>

    );
}

export default NavSidebar;
