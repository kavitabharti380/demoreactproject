import React from 'react'
import Content from '../WebsiteBuilder/Content'
import Navbar from '../WebsiteBuilder/Navbar';
import NavSidebar from './NavSidebar';

function Builder() {

    return (
        <>
            <div className="colmd1 bg-clr p-0">
                <NavSidebar />
            </div>
            <div className="colmd11 offsetmd1">
                <Navbar />
                <Content />
            </div>

        </>
    )
}

export default Builder;