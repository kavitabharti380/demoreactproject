import React from 'react'
import { faQuestionCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {Button} from 'react-bootstrap'


const Footer = () => {
    return (
        <div className="row ">
            <div className="col-md-6 offset-md-3 text-center  mt-4">

                <p style={{ backgroundColor: "rgb(169 224 230 / 15%)", fontSize: "13px", Color: "#5207ee" }} 
                    className="p-3">
                    <FontAwesomeIcon icon={faQuestionCircle} className="me-3" /> 
                    <a href=" " className="fotrclr-a" style={{ color: "#5207ee", fontWeight: "bold", fontSize: "13px" }}>Learn more</a> about designing your school in Teachable
                </p>           

            </div>
            <div className="col-12 text-end p-2 ms-4">
                    <Button variant="outline-secondary" style={{width: '10%',  background:"black", color:"white", 
                    fontSize:"13px", fontWeight:"bold", margin:"8px", textAlign:"center", border: "none"}} > 
                    <FontAwesomeIcon icon={faQuestionCircle} style={{marginLeft:"5px", fontSize:"20px", backgroundColor:"black", borderRadius:"1px solid white"}} /> Help

                    </Button>

                    
                </div>


        </div>
    )
}

export default Footer;
