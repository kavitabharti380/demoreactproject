import React from 'react'
import './cardsec.css'

function Preset(props) {
    return (
        <div className="row p-2">
            <div className="col-md-12 p-3 text-center border-xsm h100">
                <h6 className={"text-sm"}>{props.title}</h6>
                <div className="row g-2">
                    <div className="col-6 px-2" >
                        <div style={{backgroundColor: props.c1, height:"40px"}}>
                        </div>
                    </div>
                    <div className="col-6 px-2" >
                        <div style={{backgroundColor: props.c2, height:"40px"}}>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    )
}

export default Preset;
