import React from 'react'
import './content.css';

const Heading=(props) =>{
    return (
        <div className="row">
        <div className="col-md-12 mt-3">
            <h5 className="marg-txt h5">
            {props.text}
            </h5>
            {/* <p >
              Add a custom logo and/or favicon, and adjust your school thumbnail.
           </p>
             */}
        </div>
        </div>
    );
}

export default Heading;
