import React from 'react';
import Builder from './Components/WebsiteBuilder/Builder';
import Parent from './Components/Website/Parent';
import { Route } from "react-router-dom";




function App() {
  return (
    <div className="row">

      <Route path="/" component={Builder} exact/>
      <Route path="/edit" component={Parent} exact />
    </div>
  );
}

export default App;